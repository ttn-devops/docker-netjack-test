FROM ubuntu

RUN apt-get update && apt-get install jackd mplayer -y

COPY start.sh /usr/local/bin/start.sh

CMD ["start.sh"]
